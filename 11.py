# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def ordenar(lista):
    """
        Procedimiento que ordena la lista que llega como parámetro
        Como el mayor número de una lista puede ser el 9, se toma como menor
        el 10. En un ciclo se evalúa cual es el menor y va guardandose en la
        variable menor y el índice en la variable índice. Al final del ciclo
        for se agrega el valor a otra lista y se cambia el valor de este por
        10, para que no se vuelva a contar. Esto se repite hasta que el
        contador sea del largo de la lista, que es cuando evalúa todos los
        números
    """
    contador = 0
    lista_ordenada = []
    while contador < len(lista):
        menor = 10
        for i in range(len(lista)):
            if lista[i] < menor:
                menor = lista[i]
                indice = i
        lista_ordenada.append(lista[indice])
        contador += 1
        lista[indice] = 10
    print('La lista de menor a mayor es: ', lista_ordenada)


if __name__ == "__main__":
    """
        Función principal
        Genera una lista de largo aleatorio y de componentes aleatorios
        Se imprime y se envía a ordenar
    """
    lista = [random.randrange(1, 10) for z in range(1, 7)]
    print('Se ordenará de menor a mayor la siguiente lista: ', lista)
    ordenar(lista)
