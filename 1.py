# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def imprimir(lista, l):
    print('LISTA ', l)
    # se convierte la lista a cadena de caracteres
    string = ''
    for i in range(len(lista)):
        string += lista[i] + ' '
    print(string)


def verificar_repeticion(lista, i):
    return (lista.count(lista[i]))


if __name__ == "__main__":
    """
        Función principal
        A partir de la lista origen se agregan aleatoriamente palabras a una
        lista. Se envía la lista parcial a una función que analiza si la
        palabra agregada ya está en la lista. Si está repetida, se elimina
        y el índice no aumenta. Esto dentro de un ciclo que se repite hasta
        el índice sea del largo de la lista
    """
    origen = ['Di', 'buen', 'dia', 'a', 'papá']
    lista = []
    i = 0

    while i < len(origen):
        lista.append(random.choice(origen))
        veces = verificar_repeticion(lista, i)
        if veces > 1:
            del lista[i]
            i = i
        else:
            i += 1

    # se imprime la lista
    imprimir(lista, 1)
    # se invierte la lista
    lista.reverse()
    imprimir(lista, 2)