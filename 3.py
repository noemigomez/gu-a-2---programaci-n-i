# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def imprimir_laterales(triangulo, alto, ancho):
    """
        Procedimiento que traspasa la matriz de triangulos a caracter
        pasa toda la matriz y luego se utilizan los mismos ciclos de manera
        inversa. Si eran ambos ciclos del 0 al 3 (altura = 4), ahora van
        del 3 al 0. Así se forma un tipo de reflexión inferior.
        Ejemplo, altura = 2
        Si llega así:
        |\/|
        Se hace una reflexión así:
        |\/|
        |/\|
        ancho es el llegado límite usado en la función triangulos_laterales
    """
    triangulos = ''
    for i in range(alto):
        for j in range(ancho):
            triangulos += triangulo[i][j]
        triangulos += '\n'
    for i in range(alto - 1, -1, -1):
        for j in range(ancho - 1, -1, -1):
            triangulos += triangulo[i][j]
        triangulos += '\n'
    print(triangulos)


def rellena_2(triangulo, altura):
    """
        Función que pasa la matriz invertida del triangulo a caracter.
        Refleja todo hasta el límite del largo de la fila, donde comienza
        pasar de manera normal, ya que está invertida.
        Llega esto:
        /__
        Se debe pasar a esto:
        \__ y luego pasar de manera descendiente el reflejo
        \__ __/
    """
    triangulo1 = ''
    # para que no quede la primera fila \__ __/
    triangulo[0][0] = ' '
    for i in range(altura):
        for j in range(altura):
            if triangulo[i][j] == '/':
                triangulo1 += '\\'
            else:
                triangulo1 += triangulo[i][j]
            if j == (altura-1):
                for k in range(j, -1, -1):
                    triangulo1 += triangulo[i][k]
        triangulo1 += '\n'
    print(triangulo1)


def rellena(triangulo, altura):
    """
        Función que pasa la matriz de triangulo a caracter para
        imprimir. Cuando llega al límite del largo del triángulo, comienza
        con un nuevo ciclo a hacer el reflejo de la fila.
        Es decir
        altura = 3
        /__ __\
        """
    triangulo1 = ''
    for i in range(altura):
        for j in range(altura):
            triangulo1 += triangulo[i][j]
            if j == (altura-1):
                for k in range(j, -1, -1):
                    if triangulo[i][k] == '/':
                        triangulo1 += '\\'
                    else:
                        triangulo1 += triangulo[i][k]
        triangulo1 += '\n'
    print(triangulo1)


def triangulos(altura):
    """
        Procedimiento que forma el primer triángulo y luego lo invierte
        Ambas matrices formadas son enviadas a imprimir a las funciones
        rellena y rellena_2
        Se forma solo una parte del triángulo ya que luego se refleja.
        Ejemplo, altura = 3
          /
         /
        /__
    """
    triangulo = []
    for i in range(altura):
        triangulo.append([])
        for j in range(altura):
            if j == (altura-1)-i:
                triangulo[i].append('/')
            if i == (altura-1):
                triangulo[i].append('_')
            else:
                triangulo[i].append(' ')
    rellena(triangulo, altura)
    # se invierte y se envía a la función que imprime el triangulo inverso
    triangulo.reverse()
    rellena_2(triangulo, altura)


def triangulos_laterales(altura):
    """
        Crea las listas de la mitad de ambos triangulos laterales,
        en la impresión se realiza una proyección como espejo inferior
        lo que se forma es algo así, con altura = 4
        |\      /|
        | \    / |
        |  \  /  |
        |   \/   |
    """
    triangulo = []
    # limite es el ancho con ambas mitades de triangulo
    limite = (altura + 1) * 2
    for i in range(altura):
        triangulo.append([])
        for j in range(limite):
            # si j es uno de los dos extremos, se agrega |
            if j == 0 or j == limite - 1:
                triangulo[i].append('|')
            # si j está la diagonal derecha
            elif j == (limite - 1) - (i + 1):
                triangulo[i].append('/')
            # si j está en la diagonal izquierda
            elif j == limite - (limite - (i + 1)):
                triangulo[i].append('\\')
            else:
                triangulo[i].append(' ')
    # se imprimen los triangulos laterales
    imprimir_laterales(triangulo, altura, limite)


if __name__ == "__main__":
    """
        Función principal
        ingreso de altura y procedimientos que forman los triangulos
    """
    try:
        altura = int(input('Ingrese altura: '))
        # forma dos triangulos
        triangulos(altura)
        # forma triangulos laterales
        triangulos_laterales(altura)

    except ValueError:
        print('Eso no es un número.')
