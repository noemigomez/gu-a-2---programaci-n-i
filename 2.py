# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def imprime(matriz, altura):
    # se define cadena de caracteres vacía
    triangulo = ''
    # agrega los caracteres de la matriz fila por fila
    for i in range(altura):
        for j in range(altura):
            triangulo += matriz[i][j]
            """
            cuando llega al último índice comienza a recorrer la fila presente
            de atrás hacia delante formando un reflejo de la matriz generada;
            formando un triangulo. Se implementa un ciclo que va desde el
            último índice a 0. Es decir, si la fila en cuestión es: '** ' se
            copiará el reflejo quedando: '**  **'
            sería una copia de la siguiente manera:
            matriz[1][0]matriz[1][1]matriz[1][2]matriz[1][2]matriz[1][1]matriz[1][0]
            """
            if j == (altura-1):
                for k in range(j, -1, -1):
                    triangulo += matriz[i][k]
        triangulo += '\n'
    print(triangulo)


if __name__ == "__main__":
    """
        Función principal
    """
    try:
        # ingreso de altura e inicialización de matriz
        altura = int(input('Ingrese altura: '))
        matriz = []

        # en caso de que se ingrese una variable negativa
        if altura < 0:
            altura = altura * -1

        # crea una matriz solo de espacios
        for i in range(altura):
            matriz.append([])
            for j in range(altura):
                matriz[i].append(' ')

        """
        recorre fila a fila formando un triagulo de *
        si altura es 3, forma lo siguiente:
        *
        **
        ***
        """
        for i in range(altura):
            for k in range(0, i+1):
                matriz[i][k] = '*'

        # imprime el triangulo
        imprime(matriz, altura)

    except ValueError:
        print('Gracias, pero no se puede formar de dimension anumérica.')
