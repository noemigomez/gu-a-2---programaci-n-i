# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def imprimir(lista):
    for i in range(2):
        print('LISTA ', i + 1, ':', lista[i])


def eliminarrepeticiones(elegidas):
    """
        Procedimiento que elimina elementos repetidos dentro de una lista
    """
    for i in range(len(elegidas)):
        veces = 0
        for j in range(len(elegidas)):
            if elegidas[i] == elegidas[j]:
                # si se repite, aumenta el contador
                veces += 1
                if veces > 1:
                    # si está más de una vez se cambia por un '-' y se elimina
                    elegidas[j] = '-'
    for i in range(elegidas.count('-')):
        elegidas.remove('-')


def enambas_listas(palabras, lista):
    """
        Procedimiento que evalúa las palabras que están en ambas listas
    """
    ambas_listas = []
    for x in (palabras):
        a = lista[0].count(x)
        b = lista[1].count(x)
        if a > 0 and b > 0:
            ambas_listas.append(x)
    eliminarrepeticiones(ambas_listas)
    print('PALABRAS QUE SE ENCUENTRAN EN AMBAS LISTAS: ', ambas_listas)


def enlista(palabras, lista, a, b):
    """
        Procedimiento donde se se ven las repetidas entre las listas, y las
        que no lo están se agregan a una lista denominada elegidas. Esta se
        envía a un procedimiento que elimina si dentro de esta hay repetidas
    """
    elegidas = []
    for i in lista[a]:
        veces = 0
        for j in lista[b]:
            # si se encuentra en ambas lista, aumenta el contador veces
            if i == j:
                veces += 1
        if veces == 0:
            # si el contador no aumenta, significa que está en una lista
            elegidas.append(i)
    # procedimiento que elimina las repetidas dentro de una lista
    eliminarrepeticiones(elegidas)
    print('PALABRAS EN LISTA', a + 1, 'Y NO EN', b + 1, ': ', elegidas)


def analisis(palabras, lista):
    # llama a los procedimientos de los 3 análisis
    enambas_listas(palabras, lista)
    enlista(palabras, lista, 0, 1)
    enlista(palabras, lista, 1, 0)


if __name__ == "__main__":
    """
        Función principal
        se forman 2 listas a partir de la lista palabras
    """
    palabras = ['suma', 'resta', 'multiplicación', 'division',
                'potencia', 'paréntesis', 'límite', 'derivada',
                'fracción', 'trigonometría', 'conjuntos', 'logaritmo']

    lista = []
    for i in range(2):
        lista.append([])
        for j in range(6):
            lista[i].append(random.choice(palabras))

    # se envían a imprimir las listas y a analizar sus componenetes
    imprimir(lista)
    analisis(palabras, lista)
