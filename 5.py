# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def editar(list):
    # se copia la lista y a la nueva se le cambian los valores pares
    lista_final = list[:]
    for i in range(len(list)):
        if list[i] % 2 == 0 and list[i] % 10 != 0:
            lista_final[i] = random.randrange(1, 21)
        else:
            lista_final[i] = list[i]
    print('LISTA EDITADA PARES (EXCEPTO MULTIPLOS DE 10): ', lista_final)


def eliminar(list):
    """
    Se cuenta la cantidad de -. Se utiliza este valor en ciclo que los elimina
    """
    repetidos = list.count('-')
    for i in range(repetidos):
        list.remove('-')
    print('LISTA CONCATENADA: ', list)
    editar(list)


def programa(list):
    """
    Término por término evalúa cuantas veces se encuentra el número evaluado
    si se encuentra más de una vez, el siguiente se cambiará por un -
    """
    for i in range(len(list)):
        veces = 0
        for k in range(len(list)):
            if list[k] == list[i]:
                veces += 1
                if veces > 1:
                    list[k] = '-'
    # se procede a eliminar los - (numeros repetidos cambiados)
    eliminar(list)


def concatenar(cadena):
    # se concatenan las listas
    list = []
    for i in range(3):
        print('CADENA: ', cadena[i])
        list += cadena[i]
    # ejecuta el resto del programa
    programa(list)


if __name__ == "__main__":
    """
        Función principal
    """
    # creación de las 3 listas aleatoriamente (largo y contenido)
    largo_lista = random.randrange(1, 10)
    cadena = []
    for i in range(3):
        cadena.append([])
        for j in range(largo_lista):
            cadena[i].append(random.randrange(1, 21))

    # función que concatena las 3 listas
    concatenar(cadena)
