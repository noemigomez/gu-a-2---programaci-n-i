# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def moda(repeticiones, rep_palabras):
    """
        Función que recibe las listas de una fila, una con la cantidad
        de veces que aparece una palabra y otra que tiene las rep_palabras
        que aparecen, donde cada índice es equivalente a la palabra y a las
        veces que aparece dentro de la lista que se evalúa en el procedimiento
        estadistica_repeticiones
    """
    # contador inicial que evalúa cual está más veces
    mas_veces = 0
    for i in range(len(repeticiones)):
        if repeticiones[i] > mas_veces:
            """
            Si las repeticiones son mayor, se guarda la cantidad de
            repeticiones y el índice donde se encuentra
            """
            mas_veces = repeticiones[i]
            indice = i
    # se retorna la palabra del índice guardado
    return rep_palabras[indice]


def estadistica_repeticiones(lista, palabras):
    """
        Procedimiento que evalúa la palabra que más y que menos aparece
        dentro de cada lista generada
    """
    # listas simultaneas de palabras repetidas
    repeticiones = []
    rep_palabras = []
    for h in range(len(lista)):
        repeticiones.append([])
        rep_palabras.append([])
        # ciclo que recorre la lista de palabras disponibles
        for i in palabras:
            # cuantas veces está la palabra en la fila evaluada
            veces = lista[h].count(i)
            # si está la palabra, se agrega a las listas de repeticion
            if veces != 0:
                repeticiones[h].append(veces)
                rep_palabras[h].append(i)
            # si está solo una vez, se considera de los menos comunes
            if veces == 1:
                menos_comun = i
        # función que retorna la palabra más común
        mas_comun = moda(repeticiones[h], rep_palabras[h])
        print('El que más se repite en la fila', h + 1, 'es', mas_comun)
        # si el menos común existe
        try:
            print('Una palabra menos comun en fila', h + 1, 'es', menos_comun)
        except UnboundLocalError:
            print('No hay un elemento menos común.')


def eliminar_repetidos(lista):
    """
        función que elimina los -, que eran las palabras repetidas
    """
    repetidos = []
    print('------------------------------------------------------')
    for i in range(len(lista)):
        repetidos.append(lista[i].count('-'))
    for i in range(len(lista)):
        for j in range(repetidos[i]):
            lista[i].remove('-')
        print('LISTA', i + 1, ':', lista[i])


def analizar_repeticiones(lista):
    """
        función que cuando encuentra una palabra repetida, la reemplaza por -
    """
    for i in range(len(lista)):
        for j in range(len(lista[i])):
            veces = 0
            for k in range(len(lista[i])):
                if lista[i][j] == lista[i][k]:
                    veces += 1
                    if veces > 1:
                        lista[i][k] = '-'

    # procedimiento que elimina los - de palabras repetidas
    eliminar_repetidos(lista)


if __name__ == "__main__":
    """
        Función principal
        Se crea lista de palabras en la cual se basan x listas, de largo y
        x un número aleatorio entre 1 y 6
        y un número aleatorio entre 1 y 9
    """
    palabras = ['leon', 'loro', 'gato', 'perro', 'toro', 'vaca', 'pato']
    x = random.randrange(1, 7)
    y = random.randrange(1, 10)
    lista = []
    for i in range(x):
        lista.append([])
        for j in range(y):
            lista[i].append(random.choice(palabras))
    print('------------------------------------------------------')
    for i in range(x):
        print('LISTA', i + 1, ':', lista[i])

    # se envía a procedimiento que evalúa que palabras están más o menos
    estadistica_repeticiones(lista, palabras)
    # se envía a procedimiento que quita lo repetido
    analizar_repeticiones(lista)
