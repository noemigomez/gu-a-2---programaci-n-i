# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random
import time


def enunciado(enteros, k):
    # explica en el programa lo que se va a realizar
    print('Tenemos la siguiente lista: ', enteros)
    print('A partir de ella daremos 3 listas:')
    print('Con números menores a ', k)
    print('Con números mayores a ', k)
    print('Con números iguales a ', k)
    print('Los múltiplos de ', k)
    time.sleep(2)


def listas(enteros, k):
    # Se generan listas a partir de la lista de enteros por comprensión
    menores = [z for z in enteros if z < k]
    mayores = [z for z in enteros if z > k]
    iguales = [z for z in enteros if z == k]
    multiplos = [z for z in enteros if z % k == 0]
    print('MENORES: ', menores)
    print('MAYORES: ', mayores)
    print('IGUALES: ', iguales)
    print('MULTIPLOS: ', multiplos)


if __name__ == "__main__":
    """
        Función principal
    """
    # lista de enteros aleatorios
    enteros = [random.randrange(1, 21) for z in range(1, 21)]
    # k aleatorio
    k = random.randrange(1, 21)
    # procedimiento que da enunciado al programa
    enunciado(enteros, k)
    # genera las listas
    listas(enteros, k)