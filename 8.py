# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def imprimir(matriz, n, g):
    """
        Se transforma la matriz que llega en una variable str para imprimir
        lo que llega como variable g es la matriz, si es la 1 o la 2 la
        original o la inversa, para hacer la diferencia la momento de imprimir
    """
    a = ''
    for x in range(n):
        for y in range(n):
            a += matriz[x][y] + ' '
        a += '\n'
    print('Matriz ', g, ':')
    print(a)


def cambio_matriz(matriz, n):
    # copia la matriz anterior y la invierte
    matriz_nueva = matriz[:]
    for x in range(n):
        """
            La fila x = 0 será la del tamaño, ejemplo 6, menos uno
            ya que si es de tamaño 6 la fila mayor es 5
            será igual a la fila 5 - 0 de la matriz original.
            por eso, x y (n-1)-x
        """
        matriz_nueva[x] = matriz[(n - 1) - x]
    imprimir(matriz_nueva, n, 2)


# main
if __name__ == "__main__":
    """
        Función principal
        toma un número n entre 2 y 10 y forma una matriz de esa dimension
        se rellena la matriz con números de la lista números, la cual contiene
        números del 0 al 9, debido a que para luego imprimir la matriz como
        matriz se necesitaba que fueran de caracter str
    """
    n = random.randrange(2, 11)
    numeros = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    matriz = []
    for x in range(n):
        matriz.append([])
        for y in range(n):
            matriz[x].append(random.choice(numeros))

    # se imprime la matriz generada
    imprimir(matriz, n, 1)
    # procedimiento que invierte las filas
    cambio_matriz(matriz, n)
