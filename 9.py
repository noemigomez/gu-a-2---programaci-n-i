# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-
import random


def imprimir(identidad, n):
    # se imprime matriz identidad n
    a = ''
    for x in range(n):
        for y in range(n):
            a += identidad[x][y] + ' '
        a += '\n'
    print(a)


def diagonal(identidad, n):
    # diagonal con 1
    for x in range(n):
        for y in range(n):
            if x == y:
                identidad[x][y] = '1'
    imprimir(identidad, n)


if __name__ == "__main__":
    # asignación de tamaño de matriz
    n = random.randrange(1, 21)
    identidad = []
    # relleno de 0
    for x in range(n):
        identidad.append([])
        for y in range(n):
            identidad[x].append('0')

    diagonal(identidad, n)

