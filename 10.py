# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


def imprimir_mensaje(lista):
    # se invierte la lista y se pasa a una nueva variable string
    lista.reverse()
    mensaje = ''
    for i in range(len(lista)):
        mensaje += lista[i]
    print('El mensaje decifrado es: ', mensaje)


def decodificacion(lista):
    # se cuentan las X y se eliminan
    a_eliminar = lista.count('X')
    for i in range(a_eliminar):
        lista.remove('X')
    # se imprime el mensaje decodificado
    imprimir_mensaje(lista)


def a_lista(string):
    # se crea lista con todos los caracteres del mensaje
    lista = []
    for i in range(len(string)):
        lista.append(string[i])
    # procedimiento que elimina elementos de la lista
    decodificacion(lista)


if __name__ == "__main__":
    """
    Función principal
    """
    string = '¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt'
    print('Hay que decifrar el siguiente mensaje: ', string)
    # se pasa a una lista
    a_lista(string)
